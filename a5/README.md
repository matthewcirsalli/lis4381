> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Assignment 5 Requirements:

*Sub-Heading:*

1. Skillsets 13-15 Completed Programs
2. Bootstrap Validations
3. Chapter Questions (Chs 11,12,19)

#### README.md file should include the following items:

* Screenshot of Skillsets 13-15
* Screenshot of index.php
* Screenshot of each failed and passed validations
* Link to LocalHost

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

*Screenshot of Index.php*:
![Index.php Screenshot](img5/index_Screenshot.PNG)


|*Screenshot of add_petstore - Invalid*                                 |*Screenshot of add_petstore_process - Failed Validation*  |
|-----------------------------------------------------------------------|---------------------------------------------------       |
|![Invalid](img5/add_petstore_invalid_Screenshot.PNG)                   |![Failed Validation](img5/add_petstore_process_failed.PNG)                                                                                                                               |


|*Screenshot of add_petstore - Valid*                                   |*Screenshot of add_petstore_process - Passed Validation*  |
|-----------------------------------------------------------------------|---------------------------------------------------       |
|![Valid](img5/add_petstore_valid_Screenshot.PNG)                     |![Passed Validation](img5/add_petstore_process_passed_Screenshot.PNG)                                                                                                                               |


*Screenshot of Skillset 13 - Sphere Volume Calculator*:
![Skillset 13 - Sphere Volume Calculator](img5/Q13_Sphere_Volume_Calc_Screenshot.PNG "Skillset 13 Screenshot")


|*Screenshot of Skillset 14 - index.php Addition*                                |*Screenshot of Skillset 14 - process.php Addition*    |
|-----------------------------------------------------------------------|---------------------------------------------------            |
|![Addition index.php SS](img5/Q14_Simple_Calc_Index_Screenshot.PNG)    |![Addition process.php SS](img5/Q14_Simple_Calc_Process_Screenshot.PNG)                                                                                                                                    |


|*Screenshot of Skillset 14 - index.php Division*                       |*Screenshot of Skillset 14 - process.php Division* |
|-----------------------------------------------------------------------|---------------------------------------------------|
|![Division index.php SS](img5/Q14_Simple_Calc_IndexDiv_Screenshot.PNG) |![Division process.php SS](img5/Q14_Simple_Calc_ProcessDiv_Screenshot.PNG)                                                                                                                        |


|*Screenshot of Skillset 15 - index.php*                                |*Screenshot of Skillset 15 - process.php*          |
|-----------------------------------------------------------------------|---------------------------------------------------|
|![Write/Read file index](img5/Q15_Read_Write_File_index_Screenshot.PNG)|![Write/Read file process](img5/Q15_Read_Write_File_Process_Screenshot.PNG)                                                                                                                                                                                                                                                                        |

*Localhost Link*:
[Localhost Link](http://localhost:8080/repos/lis4381/ "Localhost Link")
