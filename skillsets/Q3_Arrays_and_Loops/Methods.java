import java.util.Scanner;

public class Methods 
{
 
    public static void getRequirements()
    {
        System.out.println("Developer: Matthew J. Cirsalli");
        System.out.println("Program uses loops through an array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop scructures: for, enhcanced for, while, do...while.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        System.out.println();

    }
    
    public static void arrayLoops()
    {
        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("for loop");
        for(int i = 0; i < animals.length; i++)
        {
            System.out.println(animals[i]);
        }

        System.out.println("\nEnhanced for loop");

            for(String test : animals)
            {
                System.out.println(test);
            }
        
        System.out.println("\nWhile loop");
        int i = 0;
        while (i < animals.length)
        {
            System.out.println(animals[i]);
            i++;
        }

        i = 0;
        System.out.println("\ndo... while loop:");
        do
            {

            System.out.println(animals[i]);
            i++;
            }
        while(i < animals.length); 

    }//end of array loops

}//end of class methods
