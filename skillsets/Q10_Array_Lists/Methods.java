import java.util.Scanner;
import java.util.ArrayList;


public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("Developer: Matthew J. Cirsalli");
        System.out.println("Program populates array strings with user entered animal type values.");

        System.out.println();
    }//end of getRequirements

    public static void createArrayList()
    {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>();
        String myStr = "";
        String choice = "y";
        int num = 0;

        while(choice.equals("y"))
        {
            System.out.print("Enter animal type: ");
            myStr = sc.nextLine();
            obj.add(myStr);
            num = obj.size();
            System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
            System.out.print("\nContinue? Enter y or n: ");
            choice = sc.next().toLowerCase();
            sc.nextLine();
        }
    }//end of create arraylist 

    

}//end of methods class
