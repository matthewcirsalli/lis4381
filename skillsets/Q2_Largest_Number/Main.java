import java.util.Scanner;

class Main 
{
 public static void main(String args[])   
 {
    Methods.getRequirements();
    
    
    Methods.largestNumber();

    int myNum1 = 0, myNum2 = 0;

    System.out.print("Enter first integer: ");
    myNum1 = Methods.getNum();

    System.out.print("Enter second integer: ");
    myNum2 = Methods.getNum();

    Methods.evaluateNumber(myNum1, myNum2);
 }
}
