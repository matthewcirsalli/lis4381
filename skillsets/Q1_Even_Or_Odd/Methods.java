import java.util.*;
import java.util.Scanner;
import java.lang.*;


public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Matthew Cirsalli");
        System.out.println("Program evaluates integers as Even or Odd");
        System.out.println();

    }

    public static void evaluateNumber()
    {
        int myInt = 0;
        System.out.print("Enter Integer: ");
        
        //initialize scanner
        Scanner scnr = new Scanner(System.in);

        myInt = scnr.nextInt();

        if (myInt % 2 == 0)
        {
            System.out.println(myInt + " is an even number.");
        }
        else
        {
            System.out.println(myInt + " is an odd number.");
        }

    } //end of evaluateNumber

}//end of class