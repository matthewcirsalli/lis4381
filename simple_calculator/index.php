<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Matthew's online portfolio for LIS4831.">
	<meta name="author" content="Matthew J. Cirsalli">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Simple Calculator</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Perform Calculation</h2>

						<form role="form" method="post" class="form-horizontal" action="process_functions.php">
								<div class="form-group">
										<label class="control-label col-sm-2" for="num1">Num1</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="num1" id="num1" placeholder="Enter first number"/>
										</div>
								</div>

								<div class="form-group">
										<label class="control-label col-sm-2" for="num2">Num2</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="num2" id="num2" placeholder="Enter second number"/>
										</div>
								</div>

								<div class="form-group">
										<label class="checkbox-inline">
										<div class="col-sm-12">
										<input type="radio" name="operation" id="addition" value="addition" checked="true">addition
										</label>

									<label class="checkbox-inline">
										<input type="radio" name="operation" id="subtraction" value="subtraction">subtraction
										</label>

										<label class="checkbox-inline">
										<input type="radio" name="operation" id="multiplication" value="multiplication">multiplication
										</label>

										<label class="checkbox-inline">
										<input type="radio" name="operation" id="division" value="division">division
										</label>

										<label class="checkbox-inline">
										<input type="radio" name="operation" id="exponentiation" value="exponentiation">exponentiation
										</label>

									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
									<button type="submit" class="btn btn-default">Calculate</button>
										</div>
								</div>

						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					name: {
							validators: {
									notEmpty: {
									 message: 'Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},
					
			}//end of fields
	});//end of validation
});//end of function
</script>

</body>
</html>
