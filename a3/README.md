> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Assignment 3 Requirements:

*Sub-Heading:*

1.  Skillsets 4-6 Completed Programs
2. Android Studio - My Event Application
3. Chapter Questions (Chs 5,6)

#### README.md file should include the following items:

* Screenshot of Skillsets 4-6
* Screenshot of Pets R-Us ERD
* Screenshot of each table from the ERD
* Links to a3 files

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

*Screenshot of Android Studio - Concert App opening user interface*:

![Concert App - Opening user interface](img3/myEvent_screenshot.PNG "Screenshot of opening user interface")

*Screenshot of Android Studio - Concert App processing user input*:

![Concert App - Processing user input](img3/myEvent_screenshot2.PNG "Screenshot of processing user input")

*Screenshot of MySQL ERD- Pet's R-Us ERD*:

![MySQL ERD Screenshot - Pet's R-Us](img3/pets_r_us_erd.PNG "Pet's R-Us ERD Screenshot")

*Screenshot of pet table*:

![Pet Table](img3/pet.PNG "Pet Table Screenshot")

*Screenshot of petstore table*:

![Petstore Table](img3/petstore_table.PNG "Petstore Table Screenshot")

*Screenshot of customer table*:

![Customer Table](img3/customer_table.PNG "Customer Table Screenshot")

*Screenshot of Skillset 4 - Decision Structures*:

![Skillset 4 - Decision Structures](img3/Q4_Decision_Structures.PNG "Skillset 4 Screenshot")

*Screenshot of Skillset 5 - Random Number Generator*:

![Skillset 5 - Random Number Generator](img3/Q5_Random_Number_Generator.PNG "Skillset 5 Screenshot")

*Screenshot of Skillset 6 - Methods*:

![Skillset 6 - Methods](img3/Q6_Methods.PNG "Skillset 6 Screenshot")


*a3.mwb File*:
[MySQL - a3.mwb](https://bitbucket.org/matthewcirsalli/lis4381/src/19d47fcd54d1eaf5860e607e8dfc0d6ed240dcff/a3/docs/a3.mwb "A3.mwb File")

*a3.sql File*:
[MySQL - a3.sql](https://bitbucket.org/matthewcirsalli/lis4381/src/60b11c6cb08aa686a8d3111dae82d94bc4e1dc86/a3/docs/a3.sql "A3.sql File")


