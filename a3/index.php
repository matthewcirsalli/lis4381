<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Matthew's online portfolio for LIS4381.">
		<meta name="author" content="Matthew J. Cirsalli">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> Frequently, not only will you be asked to design and develop Web applications, but you will
					also be asked to create database solutions that interact with the client/server application.
					
					A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record,
					track, and maintain relevant company data, based upon the following business rules:

					1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.
					2. A store has many pets, but each pet is sold by only one store.
				</p>

				<h4>Pets-R-Us-ERD</h4>
				<img src="img3/pets_r_us_erd.PNG" class="img-responsive center-block" alt="ERD">

				<h4>My Event - Activity 1</h4>
				<img src="img3/myEvent_screenshot.PNG" class="img-responsive center-block" alt="Android Studio Opening">

				<h4>My Event - Activity 2</h4>
				<img src="img3/myEvent_screenshot2.PNG" class="img-responsive center-block" alt="Android Studio Processing">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
