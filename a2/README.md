> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Assignment 2 Requirements:

*Sub-Heading:*

1. Created a Recipe app using Android Studio
2. Completed skillset screenshots 1-3
3. Chapter Questions (Chs 3, 4)

#### README.md file should include the following items:

* Screenshots of Screen 1 and 2 for Recipe App - Android Studio
* Screenshot of skillset 1 - Even or Odd
* Screenshot of skillset 2 - Largest Number
* Screenshot of skillset 3 - Arrays and Loops

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

*Screenshot of Bruschetta Recipe Screen 1*:

![Bruschetta Recipe Screen 1](img2/screen1.PNG "Bruschetta Recipe Screen 1")

*Screenshot of Bruschetta Recipe Screen 2*:

![Bruschetta Recipe Screen 2](img2/screen2.PNG "Bruschetta Recipe Screen 2")

*Screenshot of Skillset 1 Even or Odd*:

![Skillset 1 Even or Odd Program](img2/skillset_EvenOrOdd.PNG "Skillset 1 Even or Odd Program")

*Screenshot of Skillset 2 Largest Number*:

![Skillset 2 Largest Number Program](img2/skillset_LargestNumber.PNG "Skillset 2 Largest Number Program")

*Screenshot of Skillset 3 Arrays and Loops*:

![Skillset 3 Arrays and Loops Program](img2/skillset_ArraysAndLoops.PNG "Skillset 3 Arrays and Loops Program")
