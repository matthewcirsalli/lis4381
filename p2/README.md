> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Project 2 Requirements:

*Sub-Heading:*

1. RSS Feed addition
2. Boostrap Validations
3. Chapter Questions (Chs 13,14)

#### README.md file should include the following items:

* Screenshot of RSS Feed
* Screenshot Delete Record attempt
* Screenshot of passed and failed validations
* Link to Localhost

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

*Screenshot of Home Page*:
![Carousel Screenshot](imgp2/carousel_ss.PNG)

|*Screenshot of Index.php*                                              |*Screenshot of edit_petstore*                             |
|-----------------------------------------------------------------------|---------------------------------------------------       |
|![Index Screenshot](imgp2/p2_index_ss.PNG)                             |![edit_petstore Screenshot](imgp2/edit_ss.PNG)                                                                                                                               |


|*Screenshot of Failed Validation*                                       |*Screenshot of Passed Validation*                         |
|----------------------------------------------------------------------- |---------------------------------------------------       |
|![Failed Screenshot](imgp2/p2_failed_ss.PNG)                            |![Passed Screenshot](imgp2/p2_passed_ss.PNG)                                                                                                                                |

|*Screenshot of Delete Prompt*                                          |*Screenshot of Successful Deletion*                       |
|-----------------------------------------------------------------------|---------------------------------------------------       |
|![Delete Screenshot](imgp2/delete_prompt_ss.PNG)                       |![Successful Deletion Screenshot](imgp2/successful_delete_ss.PNG)                                                                                                                               |

*Screenshot of RSS Feed*:
![RSS Feed Screenshot](imgp2/rss_screenshot.PNG)

*Localhost Link*:
[Localhost](http://localhost:8080/repos/lis4381/ "Localhost link")



