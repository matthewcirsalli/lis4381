> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Create Bitbucket Repo
    - Provide Screenshots of installations

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Creating Healthy Recipes Android App
    - Provide Screenshots of completed app
    - Complete assigned skillset numbers 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Complete Skillsets 4-6 with screenshots
    - Provide Completed MySQL Database Screenshot
    - Provide mobile app screenshots - Android Studio

4. [P1 README.md](p1/README.md "My P1 README.md file") 
    - Complete Skillsets 7-9
    - Provide Completed My Business Card App screenshots
    - PHP/MySQL Questions (Chs. 7,8)

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Complete Skillsets 10-12
    - Test Validations with JQuery
    - Using Bootstrap

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete Skillsets 13-15
    - Test Validations JQuery
    - Create a5 using Bootstrap w/ PHP

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - RSS Feed
    - Create p2 w/ PHP
    - Test Validations JQuery