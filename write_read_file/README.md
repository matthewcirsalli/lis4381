> > **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Assignment 4 Requirements:

*Sub-Heading:*

1. Bootstrap Web Page
2. Chapter Questions (Chs 9,10,15)
3. JQuery Validation

#### README.md file should include the following items:

* Skillet Screenshots for 10-12
* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Bootsrap Web Page Completion

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

| *Screenshot of Failed Validation 1*                    | *Screenshot of failed validation 2*                                     |
| -----------                                            |               -----------                                               |
| ![Failed Validation](img4/failed_validation1.PNG)      | ![Failed Validation 2](img4/failed_validation2.PNG)                     |


*Screenshot of Passed Validation*

![Passed Validation](img4/passed_validation1.PNG "Passed Validation Screenshot")

*Screenshot of Skillset 10 - Array Lists*:

![Skillset 10 - Array Lists](img4/Q10_Array_Lists_Screenshot.PNG "Skillset 10 Screenshot")

*Screenshot of Skillset 11 - Alpha Numeric Special*:

![Skillset 11 - Alpha Numeric Special](img4/Q11_Alpha_Numeric_Special_Screenshot.PNG "Skillset 11 Screenshot")

*Screenshot of Skillset 12 - Temperature Conversion*:

![Skillset 12 - Temperature Conversion](img4/Q12_Temperature_Conversion_Screenshot.PNG "Skillset 12 Screenshot")

*Localhost Link*:
[Localhost](http://localhost:8080/repos/lis4381/ "Localhost Link")





