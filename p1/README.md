> > **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Project 1 Requirements:

*Sub-Heading:*

1. Android Studio - My Business Card App
2. Completed Skillsets 7-9
3. Chapter Questions (Chs. 7,8)

#### README.md file should include the following items:

* Screenshot of first user interface for My Business Card App
* Screenshot of second user interface for My Business Card App
* Screenshots of skillsets 7-9
* Chs 7,8 Completed

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.



#### Assignment Screenshots:

*Screenshot of Android Studio - My Business Card App opening first user interface*:

![My Business Card App - Opening First interface](imgp1/first_interface.PNG "Screenshot of opening first interface")

*Screenshot of Android Studio - Concert App processing user input*:

![My Business Card App - Second interface](imgp1/second_screenshot.PNG "Screenshot of second user interface")

*Screenshot of Skillset 7 - Random Number Generator Data Validation*:

![Skillset 7 - Random Number Generator Data Validation](imgp1/Q7_Random_Num_Gen_Data_Vali_Screenshot.PNG "Skillset 7 Screenshot")

*Screenshot of Skillset 8 - Largest Three Numbers*:

![Skillset 8 - Largest Three Numbers](imgp1/Q8_Random_Three_Numbers_Screenshot.PNG "Skillset 8 Screenshot")

*Screenshot of Skillset 9 - Array Runtime Data Validation*:

![Skillset 9 - Array Runtime Data Validation](imgp1/Q9_Array_Runtime_Data_Validation_Screenshot.PNG "Skillset 9 Screenshot")