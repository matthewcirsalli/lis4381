<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Matthew's online portfolio for LIS4381.">
		<meta name="author" content="Matthew J. Cirsalli">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> Backward-Engineer the screenshots below using your photo, contact information, and interests. 
				</p>

				<h4>My Business Card - Activity 1</h4>
				<img src="imgp1/first_interface.PNG" class="img-responsive center-block" alt="First Interface">

				<h4>My Business Card - Activity 2</h4>
				<img src="imgp1/second_screenshot.PNG" class="img-responsive center-block" alt="Second Interface">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
