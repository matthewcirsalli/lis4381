> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Matthew Cirsalli

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git innit - The git innit command creates a new Git repository
2. git status - The git status command displays the state of the working directory and the staging area.
3. git add - The git add command adds a change in the working directory to the staging area.
4. git commit - The git commit command captures a snapshot of the project's currently staged changes.
5. git push - is used to upload local repository content to a remote repository. 
6. git pull - git pull fetches the new commits and merges these into your local branch.
7. one additional git command: git checkout - is the act of switching between different versions of a target entity.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png "My AMPPS file")

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG "My JDK Output Screenshot")

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.PNG "My First App Android Studio")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/matthewcirsalli/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/matthewcirsalli/myteamquotes/ "My Team Quotes Tutorial")
